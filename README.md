# centos-e3 docker image

[Docker](https://www.docker.com) image based on Centos 7.7 to build E3 modules.
It includes the yocto ifc14xx and cct cross-compiler toolchains.

The image should use the E3_CROSS_COMPILER_SHORT_SHA as tag by default.

Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/centos-e3:latest
```
